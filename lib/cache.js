'use strict';

const EventHandler = require('@scola/events');

class MapCache extends EventHandler {
  constructor() {
    super();

    this.cache = new Map();
    this.hash = null;
  }

  getHash() {
    return this.hash;
  }

  setHash(hash) {
    this.hash = hash;
    return this;
  }

  delete(key) {
    const values = this.cache.get(this.hash);
    delete values[key];

    this.cache.set(this.hash, values);
    this.emit('change', values);

    return Promise.resolve(values);
  }

  deleteAll() {
    this.cache.delete(this.hash);
    this.emit('change');

    return Promise.resolve();
  }

  get(key) {
    const values = this.cache.get(this.hash);
    return Promise.resolve(values && values[key]);
  }

  getAll() {
    return Promise.resolve(this.cache.get(this.hash));
  }

  set(key, value) {
    const values = this.cache.get(this.hash);
    values[key] = value;

    this.cache.set(this.hash, values);
    this.emit('change', values);

    return Promise.resolve(values);
  }

  setAll(values) {
    this.cache.set(this.hash, values);
    this.emit('change', values);

    return Promise.resolve(values);
  }
}

module.exports = MapCache;
